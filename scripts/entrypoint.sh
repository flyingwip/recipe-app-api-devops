#!/bin/sh

set -e

# --noinput will surpress any questions being asked to the user
# it will go ahead and assume yes to anything
python manage.py collectstatic --noinput
# before we run migration make sure the db is available
python manage.py wait_for_db
# now run the migrations
python manage.py migrate

# start the uwsgi service 
# uwsgi => application
# --socket => run uWSGI as a TCP socket on port 9000 
## this way we can map the requests from our proxy 
## to this port using uwsgi pass (see recipe-app-api-proxy)
# --workers specify how many workers . This depends on how much memory and resources
# --master : run this as the master service on the terminal
# enable multithreading
# the module is the actual application uWSGI is going to run
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi